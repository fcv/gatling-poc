## gatling-poc - performance test suite

Project uses [sbt plugin][sbtplugindoc] of [gatling][gatlingdoc].
It contains basic simulation from gatling quick start bundle.

[sbtplugindoc]: https://gatling.io/docs/current/extensions/sbt_plugin/
[gatlingdoc]: https://gatling.io/docs/current/advanced_tutorial/

### Run

All tests:
```
sbt "gatling:test"
```

Single test:
```
sbt "gatling:testOnly computerdatabase.BasicSimulation"
```

Report:
```
sbt "gatling:lastReport"
```

### Docker

Using [`denvazh/gatling`](https://hub.docker.com/r/denvazh/gatling) image to run Gatling.

```
docker run -it --rm --entrypoint="" -v ~/tmp/gatling/user-files:/opt/gatling/user-files denvazh/gatling:3.0.3 bash
```

A `jar` file can be placed under `${GATLING_HOME}/lib` or source files can be placed under `${GATLING_HOME}/user-files/simulations`.

Note: `gatling.sh`, currently at stable version 3.2, will always [try to compile source files under `${GATLING_HOME}/user-files/simulations`](https://github.com/gatling/gatling/blob/eec8edc38c33bc1156d8561c4ee3c1be56a68d25/gatling-bundle/src/universal/bin/gatling.sh#L52).

  