import Dependencies._

enablePlugins(GatlingPlugin)

lazy val root = (project in file("."))
  .settings(
    inThisBuild(List(
      organization := "br.fcv",
      scalaVersion := "2.12.8",
      version := "0.1.0-SNAPSHOT"
    )),
    name := "gatling-poc",
    libraryDependencies ++= gatling
  )

// In order to add gatling dependencies to `main` source folder:
// libraryDependencies += "io.gatling.highcharts" % "gatling-charts-highcharts" % "3.2.0"
// libraryDependencies += "io.gatling"            % "gatling-test-framework"    % "3.2.0"
